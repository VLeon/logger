﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logger
{
    public class BDDController
    {
        public static string connectionString = ConfigurationManager.ConnectionStrings["BDD"].ConnectionString;

        public static List<BestwayReturn> RecupList()
        {
            string srRequete = string.Empty;
            List<BestwayReturn> aBestwayReturns = new List<BestwayReturn>();
            try
            {
                using (SqlConnection cnx = new SqlConnection(connectionString))
                {
                    //todo: mieux formater la requête...
                    srRequete = $"select * from \n" +
                                "(select \n" +
                                "T1.AR_Ref, \n" +
                                "T1.SAGE_Stock_Reel, \n" +
                                "isnull(PREV_ES, 0) - isnull(PREV_SS, 0) as PREV_Stock_Reel, \n" +
                                "T1.SAGE_Cde_Vente, \n" +
                                "isnull(PREV_Cde_Vente, 0) as PREV_Cde_Vente, \n" +
                                "T1.SAGE_Cde_Achat, \n" +
                                "isnull(PREV_Cde_Achat, 0) as PREV_Cde_Achat, \n" +
                                "T1.SAGE_Stock_Terme, \n" +
                                "isnull(PREV_ES, 0) - isnull(PREV_SS, 0) - isnull(PREV_Cde_Vente, 0) + isnull(PREV_Cde_Achat, 0) as PREV_Stock_Terme \n" +
                                "from \n" +
                                "(select SAGE.* \n" +
                                "from \n" +
                                "(select \n" +
                                "Ar_ref, \n" +
                                "sum(AS_QteSto) as SAGE_Stock_Reel, \n" +
                                "sum(AS_QteRes) as SAGE_Cde_Vente, \n" +
                                "sum(AS_QteCom) as SAGE_Cde_Achat, \n" +
                                "sum(AS_QteSto - AS_QteRes + AS_QteCom) as SAGE_Stock_Terme \n" +
                                " from F_ARTSTOCK \n" +
                                " group by ar_ref) \n" +
                                " SAGE \n" +
                                " inner join \n" +
                                " (select distinct PVL_Article_Ref from BIGSI_PREV.dbo.Prev_Vente_Ligne) \n" +
                                " R on R.PVL_Article_Ref = SAGE.AR_Ref) \n" +
                                " T1 \n" +
                                " left outer join \n" +
                                "   (select isnull(SUM(Pvl_qty), 0) as PREV_ES, AR_Ref \n" +
                                "from( \n" +
                                "select  Id_besoin, sum(abs(dl_qte)) as dl_qte, Ar_ref from f_docligne where DL_MvtStock = 1 \n" +
                                "and DO_DateLivr < DATEADD(day, 1, '31/08/2019') \n" +
                                " group by Ar_ref, Id_besoin)B inner join \n" +
                                "  (select * from BIGSI_PREV.dbo.Besoin_Usine BU \n" +
                                "  inner join BIGSI_PREV.dbo.Prev_Vente_Ligne PL on PL.PVL_BU_Id = BU.BU_Id and PVL_IsDeleted is null)P \n" +
                                "  on P.PVL_Article_Ref = B.AR_Ref and b.Id_besoin = p.BU_Id \n" +
                                "  group by Ar_ref) \n" +
                                "  T2 on T2.AR_Ref = T1.AR_Ref \n" +
                                "   left outer join \n" +
                                " (select isnull(isnull(S1, 0), 0) as PREV_SS, T8.AR_Ref from(select isnull(sum(abs(dl_qte)), 0) as S1, Ar_ref from F_DOCLIGNE \n" +
                                "   inner join F_DOCENTETE on F_DOCLIGNE.do_piece = F_DOCENTETE.do_piece and F_DOCENTETE.Do_type = F_DOCLIGNE.Do_type \n" +
                                " where DL_MvtStock = 3 \n" +
                                "and F_docentete.DO_DateLivr < DATEADD(day, 1, '31/08/2019')  group by Ar_ref)T8) \n" +
                                " T3 on T1.AR_Ref = T3.AR_Ref \n" +

                                " left outer join \n" +

                                "(select isnull(sum(dl_qte), 0) as PREV_Cde_Vente, AR_Ref from f_docligne \n" +
                                " inner join F_docentete on f_docligne.DO_Piece = F_DOCENTETE.DO_Piece \n" +
                                " inner join f_comptet on DO_Tiers = f_comptet.ct_num \n" +
                                " where f_docligne.do_type = 1 \n" +
                                " group by Ar_ref) \n" +
                                " T4 on T1.AR_Ref = T4.AR_Ref \n" +
                                " left outer join \n" +
                                "(select isnull(sum(p.pvl_qty), 0) as PREV_Cde_Achat, AR_Ref from(select isnull(sum(Dl_qte), 0) as S, AR_Ref, Id_besoin \n" +
                                "from F_DOCLIGNE where do_type = 12 group by AR_Ref, Id_besoin)B \n" +
                                "inner join(select * from BIGSI_PREV.dbo.Besoin_Usine BU \n" +
                                "inner join BIGSI_PREV.dbo.Prev_Vente_Ligne PL  on PL.PVL_BU_Id = BU.BU_Id  and PVL_IsDeleted is null)P \n" +
                                 "on b.Id_besoin = p.BU_Id and B.AR_Ref = P.PVL_Article_Ref group by ar_ref) T5 on T5.AR_Ref = T1.AR_Ref)Table_finale \n" +
                                 "where \n" +
                                 "--valeurs justes \n" +
                                 "--SAGE_Stock_Reel = PREV_Stock_Reel and SAGE_Cde_Vente = PREV_Cde_Vente and SAGE_Cde_Achat = PREV_Cde_Achat and SAGE_Stock_Terme = PREV_Stock_Terme \n" +

                                  "--valeurs fausse \n" +
                                  "SAGE_Stock_Reel<> PREV_Stock_Reel or SAGE_Cde_Vente<>PREV_Cde_Vente or SAGE_Cde_Achat<> PREV_Cde_Achat or SAGE_Stock_Terme<> PREV_Stock_Terme\n " +
                                    "order by AR_Ref";

                    SqlCommand cmd = new SqlCommand(srRequete, cnx);
                    cnx.Open();

                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        if (rdr.HasRows)
                        {
                            BestwayReturn currentBestwayReturn = new BestwayReturn();
                            currentBestwayReturn.ArRef = rdr.GetValue(0).ToString();
                            currentBestwayReturn.SageStockReel = Convert.ToDouble(rdr.GetValue(1).ToString());
                            currentBestwayReturn.PrevStockReel = Convert.ToDouble(rdr.GetValue(2).ToString());
                            currentBestwayReturn.SageCdeVente = Convert.ToDouble(rdr.GetValue(3).ToString());
                            currentBestwayReturn.PrevCdeVente = Convert.ToDouble(rdr.GetValue(4).ToString());
                            currentBestwayReturn.SageCdeAchat = Convert.ToDouble(rdr.GetValue(5).ToString());
                            currentBestwayReturn.PrevCdeAchat = Convert.ToDouble(rdr.GetValue(6).ToString());
                            currentBestwayReturn.SageStockTerme = Convert.ToDouble(rdr.GetValue(7).ToString());
                            currentBestwayReturn.PrevStockTerme = Convert.ToDouble(rdr.GetValue(8).ToString());

                            aBestwayReturns.Add(currentBestwayReturn);
                        }
                    }

                    return aBestwayReturns;
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"Erreur dans la méthode de récupération des données: {ex.Message}");
            }
        }

    }
}
