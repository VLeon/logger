﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Logger
{
    public static class MainController
    {
        public static void Init()
        {
            CtrlLog.DemarreLog();
            CtrlLog.EcritLog("Récupération de la liste à traiter");
            try
            {

                List<BestwayReturn> bestwayReturns = BDDController.RecupList();
                if (bestwayReturns.Count > 0)
                {
                    MethodeDeTraitement(bestwayReturns);
                }
            }
            catch (Exception ex)
            {
                CtrlLog.EcritLog(ex.Message);
            }
            CtrlLog.EcritLog("Fin du traitement");
            CtrlLog.FinLog();
        }

        private static void MethodeDeTraitement(List<BestwayReturn> bestwayReturns)
        {
            try
            {
                MailAddress receive = new MailAddress(ConfigurationManager.AppSettings["mailTo"]);
                MailAddress sender = new MailAddress(ConfigurationManager.AppSettings["mailFrom"]);
                SmtpClient smtp = new SmtpClient()
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    UseDefaultCredentials = true,
                    EnableSsl = true,
                    Credentials = new System.Net.NetworkCredential("innovationcrm2@gmail.com", "") //ajouter address et mdp sinon marche pas. Attention il faut une add google
                };

                MailMessage message = new MailMessage(sender, receive);
                string body = String.Empty;

                foreach (var item in bestwayReturns)
                {
                    body += $"<p>------------ Article: {item.ArRef} ------------<br><br>";
                    body += $"SageStockReel: {item.SageStockReel} <br>";
                    body += $"PrevStockReel: {item.PrevStockReel} <br>";
                    body += $"SageCdeVente: {item.SageCdeVente} <br>";
                    body += $"PrevCdeVente: {item.PrevCdeVente} <br>";
                    body += $"SageCdeAchat: {item.SageCdeAchat} <br>";
                    body += $"PrevCdeAchat: {item.PrevCdeAchat} <br>";
                    body += $"SageStockTerme: {item.SageStockTerme} <br>";
                    body += $"PrevStockTerme: {item.PrevStockTerme} <br><br></p>";
                }

                message.Body = body;
                message.Subject = "Erreur(s) chez Bestway";
                message.IsBodyHtml = true;

                try
                {
                    smtp.Send(message);
                    CtrlLog.EcritLog("Mail envoyé");
                }
                catch (Exception es)
                {
                    CtrlLog.EcritLog($"Exception envoi: {es.ToString()}");
                }
            }
            catch (Exception es)
            {
                CtrlLog.EcritLog($"Exception globale: {es.ToString()}");
            }
        }
    }
}
