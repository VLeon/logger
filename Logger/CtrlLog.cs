﻿using System;
using System.IO;

namespace Logger
{
    public static class CtrlLog
    {
        private static string separateur = "-----------------------------------------------------------";
        public static string filepath = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\log_bestway_" + DateTime.Now.ToString("yyyyMMdd") + ".txt";


        /// <summary>
        /// Ecrit la première partie du log
        /// </summary>
        public static void DemarreLog()
        {
            if (!Directory.Exists(Path.GetDirectoryName(filepath)))
                Directory.CreateDirectory(Path.GetDirectoryName(filepath));
            StreamWriter wr = new StreamWriter(filepath, true);
            wr.WriteLine(separateur);
            wr.WriteLine("", true);
            wr.WriteLine(DateTime.Now.ToString() + " Démarrage de l'application.", true);
            wr.Close();
        }

        /// <summary>
        /// Méthode servant à écrire un message dans le log
        /// </summary>
        /// <param name="messageLog"></param>
        public static void EcritLog(string messageLog)
        {
            StreamWriter wr = new StreamWriter(filepath, true);
            //wr.WriteLine(separateur);
            wr.WriteLine(DateTime.Now.ToString() + " :" + messageLog);
            //wr.WriteLine();
            wr.Close();
        }


        /// <summary>
        /// Ecrit à la fin du log
        /// </summary>
        public static void FinLog()
        {
            StreamWriter wr = new StreamWriter(filepath, true);
            wr.WriteLine(separateur);
            wr.WriteLine(DateTime.Now.ToString() + " :");
            wr.WriteLine("Fin du processus.");
            wr.WriteLine(separateur);
            wr.WriteLine(separateur);
            wr.Close();
        }
    }
}

