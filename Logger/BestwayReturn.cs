﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logger
{
    public class BestwayReturn
    {
        public string ArRef { get; set; }
        public double SageStockReel { get; set; }
        public double PrevStockReel { get; set; }
        public double SageCdeVente { get; set; }
        public double PrevCdeVente { get; set; }
        public double SageCdeAchat { get; set; }
        public double PrevCdeAchat { get; set; }
        public double SageStockTerme { get; set; }
        public double PrevStockTerme { get; set; }

        public BestwayReturn(string reference, double sageStockReel, double prevStockReel, double sageCdeVente, double prevCdeVente, double prevCdeAchat, double sageCdeAchat, double sageStockTerme, double prevStockTerme)
        {
            ArRef = reference;
            SageStockReel = sageStockTerme;
            PrevStockReel = prevStockReel;
            SageCdeVente = sageCdeVente;
            PrevCdeVente = prevCdeVente;
            SageCdeAchat = sageCdeAchat;
            PrevCdeAchat = prevCdeAchat;
            SageStockTerme = sageStockTerme;
            PrevStockTerme = prevStockTerme;
        }

        public BestwayReturn()
        {

        }
    }
}
